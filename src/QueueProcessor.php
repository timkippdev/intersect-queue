<?php

namespace TimKipp\IntersectQueue;

interface QueueProcessor {

    public function getFrequency();

    public function process();

}